# java-with-cucumber
This example shows examples on how to introduce a Cucumber framework for
automation tests in your organization

### Installation Steps

#### Clone the repo
`$ git clone git@gitlab.com:enterprise-tutorials/java-tutorials/java-with-cucumber.git`

#### Build the application
`$ ./gradlew clean build`

#### Run the tests
`$ ./gradlew test`

#### Launch the application
`$ ./gradlew bootRun`

* Go to homepage `curl http://localhost:8080`
* Check the health `curl http://localhost:8080/actuator/health`
* List the APIs `curl http://localhost:8080/actuator/`

## References
* [Java](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)
* [Springboot](https://spring.io/guides/gs/spring-boot/)
* [Gradle](https://gradle.org/)
* [Cucumber](https://cucumber.io/)
